@extends('layout')
@section('title', 'Owners')

@section('content')
    <h4>Owners</h4>
    <a class="btn btn-sm btn-info my-2" href="#" data-toggle="modal" data-target="#add">Add Owner</a>
    {{--modal --}}
    <div class="modal fade" id="add" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Owner Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('owners.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Fullname</label>
                            <input class="form-control" name="name" id="name" placeholder="Fullname">
                        </div>
                        <div class="form-group">
                            <label for="resort_name">Resort Name</label>
                            <input class="form-control" name="resort_name" id="resort_name" placeholder="Resort Name">
                        </div>
                        <div class="form-group">
                            <label for="contact">Contact</label>
                            <input class="form-control" name="contact" id="contact" placeholder="Contact">
                        </div>
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input class="form-control" name="location" id="location" placeholder="Location">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="Submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--end modal--}}
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if ($errors->all())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif
        <hr>
        @if (count($owners))
            <table class="table table-light table-bordered table-striped">
                <thead>
                <tr>
                    <th>Owner Name</th>
                    <th>Resort Name</th>
                    <th>Contact No.</th>
                    <th>Location</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($owners as $owner)
                    <tr>
                        <td>{{ $owner->name }}</td>
                        <td>{{ $owner->resort_name }}</td>
                        <td>{{ $owner->contact }}</td>
                        <td>{{ $owner->location }}</td>
                        <td>
                            {{--View button--}}
                                <a class="btn btn-sm btn-success" href="{{ route('owners.show', $owner->id) }}">View</a>
                            {{--Edit button--}}
                                <a class="btn btn-sm btn-primary" href="#" data-toggle="modal" data-target="#edit{{ $owner->id }}">Edit</a>
                            {{--edit modal--}}
                                <div class="modal fade" id="edit{{$owner->id}}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Owner Details</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form method="post" action="{{ route('owners.update', $owner->id) }}">
                                                @csrf
                                                @method('put')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="name">Fullname</label>
                                                        <input class="form-control" name="name" id="name" placeholder="Fullname" value="{{ $owner->name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="resort_name">Resort Name</label>
                                                        <input class="form-control" name="resort_name" id="resort_name" placeholder="Resort Name" value="{{ $owner->resort_name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="contact">Contact</label>
                                                        <input class="form-control" name="contact" id="contact" placeholder="Contact" value="{{ $owner->contact }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="location">Location</label>
                                                        <input class="form-control" name="location" id="location" placeholder="Location" value="{{ $owner->location }}">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            {{--delete button--}}
                                <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $owner->id }}">Delete</button>
                            {{--delete modal--}}
                                <div class="modal" id="delete{{ $owner->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title text-center">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p>Are you sure you want to delete?</p>

                                            <form class="d-inline-block" method="post" action="{{ route('owners.destroy', $owner->id) }}">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--register owner button--}}
                                <a class="btn btn-sm btn-primary" href="#" data-toggle="modal" data-target="#register{{ $owner->id }}">Register</a>
                            {{--register modal--}}
                            <div class="modal" id="register{{ $owner->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Owner Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                                        @if ($errors->has('name'))
                                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                                    <div class="col-md-6">
                                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                                    <div class="col-md-6">
                                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-0">
                                                    <div class="col-md-6 offset-md-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{ __('Register') }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $owners->links() }}
        @else
            <div class="" align="center">
                <h4>No data!</h4>
            </div>
        @endif
    </div>

@stop
