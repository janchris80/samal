@extends('layout')

@section('title', 'Add Hotline')

@section('content')
    <h2 class="my-3">Add Hotline</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('hotlines.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Hotline Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Hotline Name">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="contact">Contact</label>
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Submit</button>
        </div>
    </form>
@stop

