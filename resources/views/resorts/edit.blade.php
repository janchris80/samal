@extends('layout')

@section('title', 'Add Resort')

@section('content')
    <h2 class="my-3">Add Resort</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('resorts.update', $resort->id) }}" method="post">
        @csrf
        @method('put')
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $resort->name }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="contact">Contact No.</label>
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact No." value="{{ $resort->contact }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="location">location</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Location" value="{{ $resort->location }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="category">Category</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Category" value="{{ $resort->category }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="amenity">Amenity</label>
                    <input type="text" class="form-control" id="amenity" name="amenity" placeholder="Amenity" value="{{ $resort->amenity }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="10">
                        {{ $resort->description }}
                    </textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Update</button>
        </div>
    </form>
@stop
