@extends('layout')
@section('title', 'Event List')

@section('content')
    <h4>Events</h4>
    <a class="btn btn-sm btn-info my-2" href="{{ route('events.create') }}">Add Event</a>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
        @if (count($events))
            <table class="table table-light table-bordered table-striped">
                <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Event Location</th>
                    <th>Event Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($events as $event)
                    <tr>
                        <td>{{ $event->name }}</td>
                        <td>{{ $event->location }}</td>
                        <td>{{ $event->date }}</td>
                        <td>
                            @csrf
                            <a class="btn btn-sm btn-success" href="{{ route('events.show', $event->id) }}">View</a>
                            <a class="btn btn-sm btn-primary" href="{{ route('events.edit', $event->id) }}">Edit</a>
                            <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $event->id }}">Delete</button>

                            {{--delete modal--}}
                            <div class="modal" id="delete{{ $event->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title text-center">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p>Are you sure you want to delete?</p>

                                            <form class="d-inline-block" method="post" action="{{ route('events.destroy', $event->id) }}">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $events->links() }}
        @else
            <div class="" align="center">
                <h4>No data!</h4>
            </div>
        @endif
    </div>
@stop
