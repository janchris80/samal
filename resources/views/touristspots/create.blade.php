@extends('layout')

@section('title', 'Add Tourist Spot')

@section('content')
    <h2 class="my-3">Add Tourist Spot</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('touristspots.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="category">Category</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Category">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="location">location</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="location">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Submit</button>
        </div>
    </form>
@stop
