<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/home');
});
Route::get('/', 'HomeController@index');

Route::resource('events', 'EventController');
Route::resource('hotlines', 'HotlineController');
Route::resource('touristspots', 'TouristspotController');
Route::resource('resorts', 'ResortController');
Route::resource('owners', 'OwnerController');

//Route::post('');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

