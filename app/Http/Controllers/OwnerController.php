<?php

namespace App\Http\Controllers;

use App\Owner;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = Owner::orderBy('updated_at', 'desc')->paginate(10);
        return view('owners.index', ['owners' => $owners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required',
            'resort_name'       => 'required',
            'contact'           => 'required',
            'location'          => 'required',
        ]);

        Owner::create([
            'name'              => $request->name,
            'resort_name'       => $request->resort_name,
            'contact'           => $request->contact,
            'location'          => $request->location
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('owners.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function show(Owner $owner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(Owner $owner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Owner $owner)
    {
        $this->validate($request, [
            'name'              => 'required',
            'resort_name'       => 'required',
            'contact'           => 'required',
            'location'          => 'required',
        ]);

        $owner->name            = $request->name;
        $owner->resort_name     = $request->resort_name;
        $owner->contact         = $request->contact;
        $owner->location        = $request->location;
        $owner->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('owners.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Owner $owner)
    {
        $owner->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return redirect(route('owners.index'));
    }

    public function register(Request $request)
    {
        Validator::make($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        User::create([
            'name'              => $request['name'],
            'email'             => $request['email'],
            'role'              => 'Owner',
            'password'          => Hash::make($request['password']),
        ]);

        session()->flash('message', 'Successfully Register!!');

        return redirect(route('owners.index'));
    }
}
