<?php

namespace App\Http\Controllers;

use App\Hotline;
use Illuminate\Http\Request;

class HotlineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotlines = Hotline::orderBy('updated_at', 'desc')->paginate(10);
        return view('hotlines.index', ['hotlines' => $hotlines]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotlines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|min:3',
            'contact'  => 'required',
        ]);

        Hotline::create([
            'name'      => $request->name,
            'contact'  => $request->contact,
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('hotlines.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotline  $hotline
     * @return \Illuminate\Http\Response
     */
    public function show(Hotline $hotline)
    {
        return view('hotlines.show', ['hotline' => $hotline]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotline  $hotline
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotline $hotline)
    {
        return view('hotlines.edit', ['hotline' => $hotline]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotline  $hotline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotline $hotline)
    {
        $hotline->name       = $request->name;
        $hotline->contact    = $request->contact;
        $hotline->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('hotlines.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotline  $hotline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotline $hotline)
    {
        $hotline->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return redirect(route('hotlines.index'));
    }
}
